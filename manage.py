# from flask_script import Manager, Server
from flask import current_app
from flaskblog import create_app
from decouple import config

app = create_app()
# manager = Manager(app)
# manager.add_command('runserver', Server(host='127.0.0.1', port=9000))

if __name__ == '__main__':
    app.run(debug=config('DEBUG', cast=bool))
    # manager.run()