# from flask import (render_template, url_for, flash,
#                    redirect, request, abort, Blueprint)
# from flask_login import current_user, login_required
# from flaskblog import db
# from flaskblog.models import Tag, Post
# from flaskblog.tags.forms import TagForm
# from slugify import slugify

# supports = Blueprint('supports', '__name__')

# # Create support
# @tags.route('/create-tag', methods=['GET', 'POST'])
# @login_required
# def create_tag():
#     form = TagForm()
#     if form.validate_on_submit():
#         slug = slugify(form.name.data)
#         tag = Tag(name=form.name.data, slug=slug)
#         db.session.add(tag)
#         db.session.commit()
#         flash(f'"{tag.name}" has been created!', 'success')
#         return redirect(url_for('tags.create_tag'))
#     return render_template('tags/tag-form.html', title='Create Tag', form=form)

