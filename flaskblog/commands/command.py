from flask import Blueprint
import click

from getpass import getpass
from slugify import slugify
import sys

from flaskblog import db, bcrypt
from flaskblog.models import User, Category, Tag

command = Blueprint('command', __name__)

# Create user (general user)
@command.cli.command('createuser')
def create_user():
    email = input('Enter you email:\n')
    user = User.query.filter_by(email=email).first()
    while user:
        email = input(f"Error! A user already exists with this '{email}'. Please try again:\n")
        user = User.query.filter_by(email=email).first()


    username = input('Enter username:\n')
    user = User.query.filter_by(username=username).first()
    while user:
        username = input(f"Error! A user already exists with this '{username}'. Please try again:\n")
        user = User.query.filter_by(username=username).first()

    password = getpass('Password:')
    confirm_password = getpass('Confirm Password:')
    while(password != confirm_password):
        print('Password did not match! Please try again.')
        password = getpass('Password:')
        confirm_password = getpass('Confirm Password:')

    hashed_pw = bcrypt.generate_password_hash(password).decode('utf-8')
    user = User(username=username,
                email=email, password=hashed_pw)
    db.session.add(user)
    db.session.commit()

    print('Success! User is created!')

# Create superuser
@command.cli.command('createsuperuser')
def create_superuser():
    email = input('Enter you email:\n')
    user = User.query.filter_by(email=email).first()
    while user:
        email = input(f"Error! A user already exists with this '{email}'. Please try again:\n")
        user = User.query.filter_by(email=email).first()


    username = input('Enter username:\n')
    user = User.query.filter_by(username=username).first()
    while user:
        username = input(f"Error! A user already exists with this '{username}'. Please try again:\n")
        user = User.query.filter_by(username=username).first()

    password = getpass('Password:')
    confirm_password = getpass('Confirm Password:')
    while(password != confirm_password):
        print('Password did not match! Please try again.')
        password = getpass('Password:')
        confirm_password = getpass('Confirm Password:')

    hashed_pw = bcrypt.generate_password_hash(password).decode('utf-8')
    user = User(username=username,
                email=email, password=hashed_pw, is_admin=True)
    db.session.add(user)
    db.session.commit()

    print('Success! Superuser is created!')

@command.cli.command('create_categories')
def create_categories():
    categories = [
        'Python',
        'Django',
        'Flask', 
        'Php',
        'Laravel',
        'Codeigniter',
        'JavaScript',
        'Jquery',
        'Htmx',
        'Linux',
        'SQL',
        'Mysql',
        'Postgesql',
        'AWS',
        'Pynamodb'
    ]

    for cat in categories:
        is_cat_found = Category.query.filter_by(name=cat).first()
        if not is_cat_found:
            obj = Category(name=cat)
            db.session.add(obj)
            db.session.commit()
            print(f'{obj.name} is created!')


@command.cli.command('create_tag')
def create_tag():
    tags = [
        'Python',
        'Django',
        'Flask', 
        'Php',
        'Laravel',
        'Codeigniter',
        'JavaScript',
        'Jquery',
        'Htmx',
        'Linux',
        'SQL',
        'Mysql',
        'Postgesql',
        'AWS',
        'Pynamodb'
    ]

    for tag in tags:
        is_tag_found = Tag.query.filter_by(name=tag).first()
        if not is_tag_found:
            obj = Tag(name=tag.lower(), slug=slugify(tag))
            db.session.add(obj)
            db.session.commit()
            print(f'{obj.name} is created!')

    