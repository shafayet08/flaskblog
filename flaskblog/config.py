from decouple import config

class Config:
    SECRET_KEY = config('SECRET_KEY')

    # Database Configuration
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///site.db'
    if config('DEBUG', cast=bool):
        SQLALCHEMY_DATABASE_URI = config('SQLALCHEMY_DATABASE_URI_DEV')
    else:
        SQLALCHEMY_DATABASE_URI = config('SQLALCHEMY_DATABASE_URI_PROD')

    # Email Configuration
    MAIL_SERVER = config('MAIL_SERVER')
    MAIL_PORT = config('MAIL_PORT')
    MAIL_USE_TLS = config('MAIL_USE_TLS')
    MAIL_USERNAME = config('MAIL_USERNAME')
    MAIL_PASSWORD = config('MAIL_PASSWORD')

    # Geoapi 
    # GEOIPIFY_API_KEY ="at_0h5dzAeV8tUbNswceLLQos3dazKA5"

    #CKEditor Configuration
    CKEDITOR_PKG_TYPE = 'full'
    CKEDITOR_ENABLE_CODESNIPPET = True
    CKEDITOR_CODE_THEME =  'idea' #'googlecode' #'foundation'