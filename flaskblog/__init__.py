from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from flask_ckeditor import CKEditor
from flask_migrate import Migrate
from flask_simple_geoip import SimpleGeoIP
from flaskblog.config import Config


db = SQLAlchemy()
migrate = Migrate()
bcrypt = Bcrypt()


login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'



mail = Mail()
ckeditor = CKEditor()
simple_geoip = SimpleGeoIP()

from flaskblog.commands.command import command
from flaskblog.users.routes import users
from flaskblog.categories.routes import categories
from flaskblog.tags.routes import tags
from flaskblog.posts.routes import posts
from flaskblog.main.routes import main




def create_app(config_class=Config):
    app = Flask(__name__)
    app.config["GEOIPIFY_API_KEY"] = "at_0h5dzAeV8tUbNswceLLQos3dazKA5"
    db.init_app(app) 
    migrate.init_app(app, db) 
    bcrypt.init_app(app)  
    mail.init_app(app)
    login_manager.init_app(app)
    ckeditor.init_app(app)
    simple_geoip.init_app(app)

    app.config.from_object(Config)
    app.register_blueprint(command)
    app.register_blueprint(users)
    app.register_blueprint(categories)
    app.register_blueprint(tags)
    app.register_blueprint(posts)
    app.register_blueprint(main)

    return app