from flask import render_template, request, Blueprint, redirect
from flask_login import current_user, login_required
from flask_login import login_required

from flaskblog.models import Post, Category, Activity
from flaskblog.decorators import is_admin

import requests
import json

from flask import Flask, jsonify
from flaskblog import simple_geoip, db
from flaskblog.decorators import create_user_acts
main = Blueprint('main', __name__)


@main.route('/')
@main.route('/home')
@create_user_acts
def home():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.filter_by(is_public=True).order_by(
        Post.created_at.desc()).paginate(page, per_page=10)
    categories = Category.query.all()

    return render_template('home.html', home='Home', posts=posts, categories=categories)


@main.route('/about')
@create_user_acts
def about():
    return render_template('about.html', title='About')


@main.route('/search', methods=['GET'])
def search():
    page = request.args.get('page', 1, type=int)
    # Search
    search_value = request.args.get('search')

    posts = Post.query.filter(
        Post.title.contains(search_value) |
        Post.summary.contains(search_value) |
        Post.content.contains(search_value) |
        Post.tags.any(name=search_value) |
        Post.tags.any(slug=search_value)
    ).paginate(page, per_page=50)

    categories = Category.query.all()
    return render_template('home.html', title='search', posts=posts, categories=categories)


@main.route('/view-activities')
@login_required
@is_admin
def view_activities():
    # order_by(Activity.created_at.desc()
    acts = Activity.query.order_by(Activity.created_at.desc()).all()
    return render_template('activity.html', title='Activity', acts=acts)


@main.route('/get-activities', methods=['GET', 'POST'])
@login_required
@is_admin
def get_activities():
    query = Activity.query
    search = request.form.get('search[value]')
    if search:
        query = query.filter(
            Activity.ip_address.contains(search) |
            Activity.path.contains(search) |
            Activity.created_at.contains(search)
        )

    recordsFiltered = query.count()

    # pagination
    page = request.form.get('start')
    length = request.form.get('length')
    query = query.order_by(Activity.created_at.desc()
                           ).offset(page).limit(length)

    data = [item.to_dict() for item in query]
    output = {
        'draw': request.form.get('draw'),
        'recordsTotal': Activity.query.count(),
        'recordsFiltered': recordsFiltered,
        'data': data,
        'request_data': request.form.get('start')
    }
    return jsonify(output)
