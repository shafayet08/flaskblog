from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from flaskblog import db

from flaskblog.models import Post, Category
from flaskblog.categories.forms import CategoryForm
from flaskblog.decorators import is_admin

categories = Blueprint('categories', '__name__')


# Create Category
@categories.route('/create-category', methods=['GET', 'POST'])
@login_required
@is_admin
def create_category():
    form = CategoryForm()
    if form.validate_on_submit():
        category = Category(name=form.name.data)
        db.session.add(category)
        db.session.commit()
        flash(f'"{category.name}" category has been created!', 'success')
        return redirect(url_for('categories.create_category'))
    return render_template('categories/category-form.html', title='Create Category', form=form)


# Update Category
@categories.route('/update-category/<int:id>', methods=['GET', 'POST'])
@login_required
@is_admin
def update_category(id):
    category = Category.query.get_or_404(id)
    form = CategoryForm()
    if form.validate_on_submit():
        category.name = form.name.data
        db.session.commit()
        flash('Category has been Updated!', 'success')
        return redirect(url_for('main.home'))
    else:
        form.name.data = category.name
    return render_template('categories/category-form.html', title='Update Category', form=form)

# Category details
@categories.route('/category-detail/<int:id>', methods=['GET'])
def category_detail(id):
    category = Category.query.get(id)
    return render_template('categories/category-detail.html', title='Categories', category=category)
