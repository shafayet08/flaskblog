from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from flaskblog import db
from flaskblog.models import Post, Category, Tag, Like, Comment
from flaskblog.posts.forms import PostForm, CommentForm, ReplyForm
from slugify import slugify

posts = Blueprint('posts', __name__)


# Create Post
@posts.route('/create-post', methods=['GET', 'POST'])
@login_required
def create_post():
    tags = Tag.query.all()
    form = PostForm()
    form.category.choices = [(g.id, g.name)
                             for g in Category.query.order_by('name')]
    if form.validate_on_submit():
        post = Post(title=form.title.data, summary=form.summary.data,
                    content=form.content.data, is_public=form.public.data,
                    category_id=form.category.data, author=current_user)

        tags = request.form.getlist('tags')
        for item in tags:
            tag = Tag.query.get(item)
            print(item)
            post.tags.append(tag)

        db.session.add(post)
        db.session.commit()
        flash('Your post has been created!', 'success')
        return redirect(url_for('main.home'))

    return render_template('posts/post-form.html', title='Create Post', form=form, tags=tags)


@posts.route('/post-detail/<int:id>', methods=['GET', 'POST'], strict_slashes=False)
def post_detail(id):
    post = Post.query.filter_by(id=id).first_or_404()
    commentForm = CommentForm()
    replyForm = ReplyForm()
    comments = Comment.query.filter_by(
        post_id=post.id).order_by(Comment.path.asc())

    if commentForm.validate_on_submit():
        text = commentForm.comment.data

        comment = Comment(
            text=text,
            user_id=current_user.id,
            post_id=post.id,
        )

        comment.save()
        flash("Your comment has been created! ", "success")
        return redirect(url_for('posts.post_detail', id=id))
    return render_template('posts/post-detail.html',
                           title='Post Detail',
                           post=post,
                           commentForm=commentForm,
                           replyForm=replyForm,
                           comments=comments,)


@posts.route("/<int:post_id>/<int:comment_id>", methods=("GET", "POST"), strict_slashes=False)
@login_required
def reply_comment(post_id, comment_id):
    replyForm = ReplyForm()
    post = Post.query.get_or_404(post_id)
    parent = Comment.query.get_or_404(comment_id)

    if replyForm.validate_on_submit():
        text = replyForm.reply.data

        comment = Comment(
            parent=parent,
            text=text,
            swap_id='',
            user_id=current_user.id,
            post_id=post.id,
        )
        comment.save()
    replyForm.reply.data = None
    return render_template('posts/partial/reply.html', replyForm=replyForm, comment=comment, post=post)


@posts.route('/like_or_dislike/<int:id>', methods=['POST'])
@login_required
def like_or_dislike(id):
    post = Post.query.get_or_404(id)
    # like = any(like.user.id == current_user.id for like in post.likes)
    like_id = None
    for like in post.likes:
        if like.user.id == current_user.id:
            like_id = like.id
            break
    if like_id:
        db.session.delete(Like.query.get(like_id))
        total_likes = post.total_likes or 0
        post.total_likes = total_likes - 1
    else:
        like = Like(post_id=id, user_id=current_user.id)
        db.session.add(like)
        total_likes = post.total_likes or 0
        post.total_likes = total_likes + 1
    db.session.commit()

    return render_template('posts/partial/like.html', post=post)


@posts.route('/post-update/<int:id>', methods=['GET', 'Post'])
@login_required
def post_update(id):
    post = Post.query.get_or_404(id)
    if post.author != current_user:
        abort(403)
    form = PostForm()
    form.category.choices = [(c.id, c.name)
                             for c in Category.query.order_by('name')]

    if form.validate_on_submit():
        post.title = form.title.data
        post.summary = form.summary.data
        post.content = form.content.data
        post.is_public = form.public.data
        post.category_id = form.category.data
        post.user = current_user

        tags = request.form.getlist('tags')
        updated_tags = list()
        if tags:
            for item in tags:
                tag = Tag.query.get(item)
                if tag:
                    updated_tags.append(tag)
        post.tags = updated_tags

        db.session.commit()

        flash('Your post has been Updated!', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        form.category.data = post.category_id
        form.title.data = post.title
        form.summary.data = post.summary
        form.content.data = post.content
        form.public.data = post.is_public
        tags = Tag.query.all()

    return render_template('posts/post-form.html', title='Post Update', form=form, tags=tags, post=post)



# htmx request
@posts.route('/post-delete/<int:id>', methods=['POST'])
@login_required
def post_delete(id):
    post = Post.query.get_or_404(id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    return ''


