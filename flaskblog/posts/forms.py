from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, BooleanField, SelectField, SelectMultipleField
from wtforms.validators import DataRequired
from flask_ckeditor import CKEditorField

from flaskblog.models import User


class PostForm(FlaskForm):
    category = SelectField(u'Category', coerce=int,
                           validators=[DataRequired()])
    title = StringField('Title', validators=[DataRequired()])
    summary = TextAreaField('Summary', validators=[DataRequired()])
    # content = TextAreaField('Content', validators=[DataRequired()])
    content = CKEditorField('Content', validators=[DataRequired()])
    public = BooleanField('Public', default=True)
    submit = SubmitField('Submit')

class CommentForm(FlaskForm):
    comment = TextAreaField(validators=[DataRequired()])


class ReplyForm(FlaskForm):
    reply = TextAreaField(validators=[DataRequired()])
