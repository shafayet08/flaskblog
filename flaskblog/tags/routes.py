from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from flaskblog import db
from flaskblog.models import Tag, Post
from flaskblog.tags.forms import TagForm
from slugify import slugify

tags = Blueprint('tags', '__name__')

# Create tag
@tags.route('/create-tag', methods=['GET', 'POST'])
@login_required
def create_tag():
    form = TagForm()
    if form.validate_on_submit():
        slug = slugify(form.name.data)
        tag = Tag(name=(form.name.data).lower(), slug=slug)
        db.session.add(tag)
        db.session.commit()
        flash(f'"{tag.name}" has been created!', 'success')
        return redirect(url_for('tags.create_tag'))
    return render_template('tags/tag-form.html', title='Create Tag', form=form)


@tags.route('/update-tag/<int:id>', methods=['GET', 'POST'])
@login_required
def update_tag(id):
    tag = Tag.query.get_or_404(id)
    form = TagForm()
    if form.validate_on_submit():
        tag.slug = slugify(form.name.data)
        tag.name = (form.name.data).lower()
        db.session.commit()
        flash('Tag has been Updated!', 'success')
        return redirect(url_for('main.home'))
    else:
        form.name.data = tag.name
    return render_template('tags/tag-form.html', title='Update tag', form=form)


@tags.route('/post-by-tag/<slug>', methods=['GET'])
def post_by_tag(slug):
    tag = Tag.query.filter_by(slug=slug).first()
    posts = Post.query.filter(Post.tags.any(slug=slug))
    return render_template('tags/post-by-tag.html', title='Posts by tag', tag=tag, posts=posts)
