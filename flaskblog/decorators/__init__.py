from flask import url_for, current_app, session, request, redirect, flash
from flask_login import current_user

import functools

from flaskblog import db
from flaskblog.models import Activity

def create_act():
    act = None
    if not current_user.is_authenticated:
        act = Activity(
            ip_address=request.remote_addr,
            path=request.path,
            is_anonymous=True
        )
        session['is_anonymous'] = True
    if current_user.is_authenticated:
        act = Activity(
            ip_address=request.remote_addr,
            path=request.path,
            user_id=current_user.id,
            is_anonymous=False
        )
    if act is not None:
        session['is_visited'] = True
        db.session.add(act)
        db.session.commit()
        print('*******  Act is created! ********')


def create_user_acts(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        try:
            is_visited = session['is_visited']
        except:
            is_visited = False
        if not is_visited or request.path == '/about':
            create_act()
        return view(**kwargs)
    return wrapped_view


def is_admin(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.is_authenticated:
            flash('Please login!.', 'info')
            return redirect(url_for('users.login'))
        else: 
            if not current_user.is_admin:
                flash('Please login as admin.', 'info')
                return redirect(url_for('users.login'))
        return view(**kwargs)
    return wrapped_view