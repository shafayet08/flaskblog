$(document).ready(function() {
    $('.js-tag').select2();

    var loc = window.location.pathname;

    $('#nav-item').find('a').each(function() {
      $(this).toggleClass('active-item', $(this).attr('href') == loc);
   });

   // Comment Page-  for replying
   $( ".reply" ).submit(function( event ) {
      event.preventDefault();
      $(this).parent().removeClass('show')
    });
});