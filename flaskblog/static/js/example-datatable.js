$(document).ready(function () {
    $('#example-datatable').DataTable({
        "responsive": {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll()
            }
        },
        "stateSave": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/get-activities',
            type: 'POST',
            // success: (res)=>{
            //     console.log(res)
            // },
            // error: (e) => {
            //     console.log(e)
            // },
        },
    });
});
