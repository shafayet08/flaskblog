from flask import url_for, current_app, session, request, redirect, flash
from flask_login import current_user
from flask_mail import Message

import os
import functools
import secrets
from PIL import Image
from datetime import datetime

from flaskblog import mail, db
from flaskblog.models import Activity


def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(
        current_app.root_path, 'static/profile_pic', picture_fn)
    form_picture.save(picture_path)

    output_size = (300, 300)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


def send_password_reset_mail(user):
    token = user.get_reset_token()
    msg = Message(
        'Password Rest Request',
        sender='noreply@shafayet.com',
        recipients=[user.email]
    )
    msg.body = f'''
            To reset your password, visit the following link:
            {url_for('users.reset_password_token', token=token, _external=True)}
            If you did not make this request then simply ignore this mail and 
            no changes will take effect.
            '''
    mail.send(msg)


def create_act():
    act = None
    if not current_user.is_authenticated:
        act = Activity(
            ip_address=request.remote_addr,
            path=request.path,
            is_anonymous=True
        )
        session['is_anonymous'] = True
    if current_user.is_authenticated:
        act = Activity(
            ip_address=request.remote_addr,
            path=request.path,
            user_id=current_user.id,
            is_anonymous=False
        )
    if act is not None:
        session['is_visited'] = True
        db.session.add(act)
        db.session.commit()
        print('*******  Act is created! ********')


def create_user_acts(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        try:
            is_visited = session['is_visited']
        except:
            is_visited = False
        if not is_visited or request.path == '/about':
            create_act()
        return view(**kwargs)
    return wrapped_view


def is_admin(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.is_authenticated:
            flash('Please login!.', 'info')
            return redirect(url_for('users.login'))
        else: 
            if not current_user.is_admin:
                flash('Please login as admin.', 'info')
                return redirect(url_for('users.login'))
        return view(**kwargs)
    return wrapped_view
