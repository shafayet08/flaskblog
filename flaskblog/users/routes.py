from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from flaskblog import db, bcrypt
from flaskblog.models import User, Post
from flaskblog.users.forms import (UserRegistrationForm, LoginForm, UserUpdateForm,
                                   PasswordResetRequestForm, PasswordResetForm)
from flaskblog.users.utils import save_picture, send_password_reset_mail
users = Blueprint('users', __name__)


@users.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = UserRegistrationForm()
    if form.validate_on_submit():
        hashed_pw = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = User(username=form.username.data,
                    email=form.email.data, password=hashed_pw)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in.', 'success')
        return redirect(url_for('users.login'))
    context = {
        'title': 'User Registration',
        'form': form,
    }
    return render_template('accounts/register.html', form=form)


@users.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.rememberme.data)
            flash('Login Successful!', 'success')
            next_page = request.args.get('next')

            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        else:
            flash('Login Unsuccessful! Please provide valid credentials.', 'danger')

    return render_template('accounts/login.html', title='Login', form=form)


@users.route('/update_profile', methods=['GET', 'POST'])
def update_profile():
    form = UserUpdateForm()
    if form.validate_on_submit():
        if form.image.data:
            picture_file = save_picture(form.image.data)
            print(picture_file)
            current_user.image = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your profile has been updated!', 'success')
        return redirect(url_for('users.profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    return render_template('accounts/update-profile.html', title='Update Profile', form=form)


@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('users.login'))


@users.route("/profile")
@login_required
def profile():
    image = url_for('static', filename='profile_pic/' + current_user.image)
    page = request.args.get('page', 1, type=int)
    posts = Post.query.filter_by(author=current_user)\
        .order_by(Post.created_at.desc())\
        .paginate(page, per_page=10)
    # return render_template('user-posts.html', home='Home', posts=posts, user=user)

    return render_template('accounts/profile.html', title=current_user.username, posts=posts, image=image)


@users.route('/password/reset/request', methods=['GET', 'POST'])
def password_reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = PasswordResetRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_password_reset_mail(user)
        flash('An email has been sent with instruction to reset your password!', 'success')
        return redirect(url_for('users.login'))
    return render_template('accounts/password-reset-request.html', title='Password Reset Request', form=form)


@users.route('/reset/password/<token>', methods=['GET', 'POST'])
def reset_password_token(token):
    if current_user.is_authenticated:
        return redirect('home')
    user = User.verify_reset_token(token)
    if user is None:
        flash('This is an invalid or expired token', 'danger')
        return redirect(url_for('users.password_reset_request'))
    form = PasswordResetForm()
    if form.validate_on_submit():
        hashed_pw = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user.password = hashed_pw
        db.session.commit()
        flash('Your account has been updated! You are now able to log in.', 'success')
        return redirect(url_for('users.login'))
    return render_template('accounts/reset-password-token.html', title='Reset Password', form=form)
