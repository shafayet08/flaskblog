
from flask import current_app
from datetime import datetime
from flaskblog import db, login_manager
from flask_login import UserMixin, current_user
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from slugify import slugify


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    image = db.Column(db.String(1000), nullable=False, default='avatar.png')
    is_admin = db.Column(db.Boolean, default=False)
    posts = db.relationship('Post', backref='author', lazy=True)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__self(self):
        return f"User('{self.username}', '{self.email}', '{self.image}')"


class Activity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String(30))
    path = db.Column(db.String(100), nullable=True)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                            nullable=True)
    user = db.relationship('User',
                               backref=db.backref('acts', lazy=True))
    is_anonymous = db.Column(db.Boolean, nullable=False)

    def to_dict(self):
        # username = item.user.username if item.user else ''
        if self.user:
            username = self.user.username
        else:
            username = ''
        # username = item.user.username if item.user else '' 
        return {
            0: self.id,
            1: self.ip_address,
            2: self.path,
            3: username,
            4: self.created_at
        }

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)

    def __repr__self(self):
        return self.name

    @property
    def total_posts(self):
        return Post.query.filter_by(category_id=self.id, is_public=True).count()


class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    slug = db.Column(db.String(50), unique=True, nullable=False)

    # def __init__(self):
    #     # super().__init__(self, *args, **kwargs)
    #     self.slug = slugify(self.name)

    def __repr__self(self):
        return self.name


posts_tags = db.Table('posts_tags',
                      db.Column('post_id', db.Integer,
                                db.ForeignKey('post.id')),
                      db.Column('tag_id', db.Integer,
                                db.ForeignKey('tag.id')),
                      )


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    summary = db.Column(db.Text(400), nullable=False)
    content = db.Column(db.Text, nullable=False)
    is_public = db.Column(db.Boolean, default=True)
    total_likes = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'),
                            nullable=False)
    category = db.relationship('Category',
                               backref=db.backref('posts', lazy=True))
    tags = db.relationship('Tag', secondary=posts_tags,
                           backref=db.backref('posts', lazy='dynamic'))

    def __repr__self(self):
        return f"Post('{self.title}', '{self.created_at}')"

    @property
    def user_has_like(self):
        return Like.query.filter_by(user_id=current_user.id, post_id=self.id).first()


class Like(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'),
                        nullable=False)
    post = db.relationship('Post', backref=db.backref('likes', lazy=True))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                        nullable=False)
    user = db.relationship('User', backref=db.backref('likes', lazy=True))


class Comment(db.Model):
    MAX_LEVEL = 6

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(200))

    # author = db.Column(db.String(32))
    created_at = db.Column(db.DateTime(), default=datetime.utcnow, index=True)
    path = db.Column(db.Text, index=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('comments', lazy=True))

    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    post = db.relationship('Post', backref=db.backref('comments', lazy=True))
    parent_id = db.Column(db.Integer, db.ForeignKey('comment.id'))
    # Only used for html
    swap_id = db.Column(db.Text, default='')
    replies = db.relationship(
        'Comment', backref=db.backref('parent', remote_side=[id]),
        lazy='dynamic')

    def save(self):
        db.session.add(self)
        db.session.commit()
        prefix = self.parent.path + '.' if self.parent else ''
        self.path = prefix + '{:0{}d}'.format(self.id, self.MAX_LEVEL)
        db.session.commit()

    def level(self):
        return len(self.path) // self.MAX_LEVEL - 1
